import json

def count(filename)->int:
    #compte nombre film
    with open(filename, "r", encoding="utf-8") as file: #ouvre le fichier
        listOfFilm = json.load(file)                    #prend l'objet fichier comme paramètre.
        numberOfFilm = len(listOfFilm)                  #est égal à la taille du nombre de mini listes
    return numberOfFilm

def written_by(filename, writer):
    #liste film de l'auteur nommé
    result = []
    with open(filename, "r", encoding="utf-8") as file:
        listOfFilm = json.load(file)
        for i in range(len(listOfFilm)):                #pour le nombre de film dans le fichier film.JSON
            nameFound = (listOfFilm[i]["Writer"])       #le nom trouvé est égal au nom de l'écrivain du film n°i
            if (nameFound==writer):                     #si le nom trouvé et le même que celui recherché
                result.append(listOfFilm[i])            #ajout du film dans la liste result
    return result

def longest_title(filename):
    #cherche le film avec le nom le plus long
    result = []
    nameOfFilm = "a"
    with open(filename, "r", encoding="utf-8") as file:
        listOfFilm = json.load(file)
        for i in range(len(listOfFilm)):
            sizeNameFound = len(listOfFilm[i]["Title"]) #initialisation de la taille du titre trouvé
            if (sizeNameFound>len(nameOfFilm)):         #si la taille du titre trouvé est plus grand que le précédent
                nameOfFilm = listOfFilm[i]["Title"]
                result.append(listOfFilm[i]["Title"])
    return result
#COMPRENDS PAS ERREUR : list indices must be integers or slices, not str
#OUBLIE PAS DE METTRE DES # POUR PAS SKIP

def best_rating(filename):
    result = []
    #cherche l'enregistrement avec le meilleur imdbRating
    bestGrade = 0.0
    with open(filename, "r", encoding="utf-8") as file:
        listOfFilm = json.load(file)
        for i in range(len(listOfFilm)):
            gradeFound = listOfFilm[i]["imdbRating"]     #le note trouvé est égal au grade du film n°i
            int_bestGrade = int(bestGrade)
            if (int_bestGrade > bestGrade):                 #si le note trouvé est supérieur au précédent
                bestGrade = gradeFound
                result.append(listOfFilm[i]["Title"])    #le nom du film avec la meilleur note est celui trouvé
    return result
#OUBLIE PAS DE METTRE DES # POUR PAS SKIP

def latest_film (filename):
    result = []
    #cherche l'enregistrement avec le Year le plus récent, retourne le nom du film concerné
    with open(filename, "r", encoding="utf-8") as file:
        listOfFilm = json.load(file)
        for i in range(len(listOfFilm)):
            print(listOfFilm[i]["Year"])
    return result

'''
Brouillon :

...
    POUR i JUSQU'A FIN lisOfFilm
        valeur = listOfFilm[i]["Year"]
        int_valeur = int(valeur)
        
        SI i = 0 ALORS
            année_la_plus_proche_de_2020 = int_valeur
            
        SINON SI int_valeur > année_la_plus_proche_de_2020 ALORS
            année_la_plus_proche_de_2020 = int_valeur
            result.append(listOfFilm[i]["Title"])
        FIN SI
    FIN POUR
return result

'''